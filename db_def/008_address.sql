CREATE TABLE `address` (
  `addID` int(11) NOT NULL AUTO_INCREMENT,
  `addStreet` varchar(255) DEFAULT NULL,
  `addPLZ` varchar(15) DEFAULT NULL,
  `addCountry` varchar(5) DEFAULT NULL,
  `addType` enum('visitor','post') DEFAULT NULL,
  `addDefault` int(1) DEFAULT 0,
  `addOrt` varchar(255) DEFAULT NULL,
  `addName` varchar(255) DEFAULT NULL,
  `addRegion` varchar(100) DEFAULT NULL,
  `addLandkreis` varchar(100) DEFAULT NULL,
  `addMD5` varchar(32) GENERATED ALWAYS AS (md5(concat_ws('',`addStreet`,`addPLZ`,`addCountry`,`addType`,`addDefault`,`addOrt`,`addName`,`addRegion`,`addLandkreis`))) VIRTUAL,
  PRIMARY KEY (`addID`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8mb4