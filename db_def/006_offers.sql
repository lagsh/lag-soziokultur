CREATE TABLE `offers` (
  `offID` int(11) NOT NULL AUTO_INCREMENT,
  `offType` varchar(45) DEFAULT NULL,
  `offUrl` varchar(255) DEFAULT NULL,
  `offDescription` longtext DEFAULT NULL,
  PRIMARY KEY (`offID`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4