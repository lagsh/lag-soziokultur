CREATE TABLE `event_categorie` (
  `evID` int(11) NOT NULL,
  `catID` int(11) NOT NULL,
  `gemappt` int(1) DEFAULT 0,
  PRIMARY KEY (`evID`,`catID`),
  KEY `evcatcatID_idx` (`catID`),
  CONSTRAINT `evcatcatID` FOREIGN KEY (`catID`) REFERENCES `categorie` (`catID`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `evcatevID` FOREIGN KEY (`evID`) REFERENCES `event` (`evID`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4