CREATE TABLE `event_images` (
  `evID` int(11) NOT NULL,
  `imgID` int(11) NOT NULL,
  PRIMARY KEY (`evID`,`imgID`),
  KEY `event_images_imgID_idx` (`imgID`),
  CONSTRAINT `event_images_evID` FOREIGN KEY (`evID`) REFERENCES `event` (`evID`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `event_images_imgID` FOREIGN KEY (`imgID`) REFERENCES `images` (`imgID`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4