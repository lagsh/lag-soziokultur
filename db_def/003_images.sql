CREATE TABLE `images` (
  `imgID` int(11) NOT NULL AUTO_INCREMENT,
  `imgLicense` varchar(255) DEFAULT NULL,
  `imgCreator` varchar(255) DEFAULT NULL,
  `imgCaption` varchar(255) DEFAULT NULL,
  `imgDescription` varchar(255) DEFAULT NULL,
  `imgUrl` varchar(255) NOT NULL,
  `imgUrlMD5` varchar(32) NOT NULL,
  PRIMARY KEY (`imgID`)
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=utf8mb4