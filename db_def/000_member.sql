CREATE TABLE `member` (
  `meID` int(11) NOT NULL AUTO_INCREMENT,
  `meIDorg` varchar(20) NOT NULL,
  `meName` varchar(255) DEFAULT NULL,
  `meEmail` varchar(255) DEFAULT NULL,
  `meUrl` varchar(255) DEFAULT NULL,
  `meDescription` longtext DEFAULT NULL,
  `meFoundingDate` varchar(45) DEFAULT NULL,
  `meTelephone` varchar(45) DEFAULT NULL,
  `meTeaser` longtext DEFAULT NULL,
  PRIMARY KEY (`meID`),
  UNIQUE KEY `meIDorg_UNIQUE` (`meIDorg`),
  UNIQUE KEY `meID_UNIQUE` (`meID`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4