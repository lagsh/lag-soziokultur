CREATE TABLE `member_merkmale` (
  `meID` int(11) NOT NULL,
  `merID` int(11) NOT NULL,
  PRIMARY KEY (`meID`,`merID`),
  KEY `memmerkmerID_idx` (`merID`),
  CONSTRAINT `memmerkmeID` FOREIGN KEY (`meID`) REFERENCES `member` (`meID`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `memmerkmerID` FOREIGN KEY (`merID`) REFERENCES `merkmale` (`merID`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4