CREATE TABLE `location` (
  `locID` int(11) NOT NULL AUTO_INCREMENT,
  `locType` varchar(45) DEFAULT NULL,
  `locLng` float(9,6) DEFAULT NULL,
  `locLat` float(9,6) DEFAULT NULL,
  `locUrl` varchar(255) DEFAULT NULL,
  `locName` varchar(255) DEFAULT NULL,
  `locDescription` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`locID`)
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=utf8mb4