<?php
declare(strict_types = 1);

namespace part\lag\db;
use part\lag\clLAGAPIBase;
use part\mariadb\clMariaDB;


class clLAGDB {
	static $db_def_dir = WP_PLUGIN_DIR . '/part-lag-soziokultur-data/db_def/';

	static $tables = ["address", "categorie", "event", "event_address", "event_categorie", "event_images", "event_location", "event_merkmale",
		"event_offers", "event_tags", "image", "images", "location", "member", "member_address", "member_images", "member_location",
		"member_merkmale", "member_tags", "merkmale", "offers", "tag"];

	static function saveToTable(object $Data, array $saveFields, string $Table) : int {
		$fields = [];
		$values = [];
		foreach ($saveFields as $dbField => $objField){
			if(isset($Data->{$objField})){
				switch (gettype($Data->{$objField})){
					case "string":
						$fields[] = $dbField;
						$values[] = clMariaDB::escapeStr($Data->{$objField});
						break;
					case "integer":
					case "double":
						$fields[] = $dbField;
						$values[] = $Data->{$objField};
				}
			}
			//print_r(array_keys(get_object_vars($Data)));
			//print_r(array_diff(array_keys(get_object_vars($Data)), array_values($saveFields)));
		}
		$SQL = "insert into $Table (" . implode(", ", $fields) . ") values ('" . implode("', '", $values) . "')";
		clMariaDB::query($SQL);
		return clMariaDB::insert_id();
	}
	static function saveMember(object $Data, array $saveFields) : int{
		return self::saveToTable($Data, $saveFields, "member");
	}

	/*******************Event***************************/
	public static function saveEvent(object $Data, array $saveFields) : int {
		return self::saveToTable($Data, $saveFields, "event");
	}
	/*******************Event***************************/
	/*******************Kategorien***************************/
	public static function saveKategorien(int $ID, array $Kats, bool $gemappt) {
		if(count($Kats) == 0){
			return;
		}
		$values = [];
		foreach ($Kats as $Kat){
			$cat = self::getKategorie($Kat);
			if(!$cat){
				self::newKategorie($Kat);
				$cat = self::getKategorie($Kat);
			}
			$values[] = "($ID, $cat->catID, " . ($gemappt ? 1 : 0) . ")";
		}
		$SQL = "insert into event_categorie (evID, catID, gemappt) values " . implode(", ", $values);
		clMariaDB::query($SQL);
	}

	static function getKategorie(string $Name){
		clMariaDB::escapeStr($Name);
		$SQL = "select * from categorie where catName = '$Name' ";
		$Merkmal = clMariaDB::queryObject($SQL, 1 );
		return count($Merkmal) > 0 ? $Merkmal[0] : null;
	}

	static function newKategorie(string $Name) : int {
		clMariaDB::escapeStr($Name);
		$SQL = "insert into categorie (catName) values ('$Name')";
		clMariaDB::query($SQL);
		return clMariaDB::insert_id();
	}

	/*******************Kategorien***************************/

	/*******************Merkmale***************************/
	const MerkmaleMember = 1;
	const MerkmaleEvent = 2;
	public static function saveMerkmaleMember(int $ID, array $Merkmale) : void{
		self::saveMerkmale($ID, $Merkmale, self::MerkmaleMember);
	}
	public static function saveMerkmaleEvent(int $ID, array $Merkmale) : void{
		self::saveMerkmale($ID, $Merkmale, self::MerkmaleEvent);
	}
	private static function saveMerkmale(int $ID, array $Merkmale, int $Typ) : void{
		if(count($Merkmale) == 0){
			return;
		}
		$values = [];
		foreach ($Merkmale as $Merkmal){
			$merk = self::getMerkmal($Merkmal);
			if(!$merk){
				self::newMerkmal($Merkmal);
				$merk = self::getMerkmal($Merkmal);
			}
			$values[] = "($ID, $merk->merID)";
		}
		if($Typ == self::MerkmaleMember){
			$SQL = "insert into member_merkmale (meID, merID) values " . implode(", ", $values);
		}elseif ($Typ == self::MerkmaleEvent){
			$SQL = "insert into event_merkmale (evID, merID) values " . implode(", ", $values);
		}
		clMariaDB::query($SQL);

	}
	static function newMerkmal(string $Name) : int {
		clMariaDB::escapeStr($Name);
		$SQL = "insert into merkmale (merName) values ('$Name')";
		clMariaDB::query($SQL);
		return clMariaDB::insert_id();
	}
	static function getMerkmal(string $Name){
		clMariaDB::escapeStr($Name);
		$SQL = "select * from merkmale where merName = '$Name' ";
		$Merkmal = clMariaDB::queryObject($SQL, 1);
		return count($Merkmal) > 0 ? $Merkmal[0] : null;
	}
	/*******************Merkmale***************************/
	/*******************Tags***************************/
	const TagMember = 1;
	const TagEvent = 2;
	public static function saveTagMember(int $ID, array $Tags) {
		self::saveTag($ID, $Tags, self::TagMember);
	}
	public static function saveTagEvent(int $ID, array $Tags) {
		self::saveTag($ID, $Tags, self::TagEvent);
	}
	private static function saveTag(int $ID, array $Tags, int $Typ) : void{
		if(count($Tags) == 0){
			return;
		}
		$values = [];
		foreach ($Tags as $Tag){
			$clTag = self::getTag($Tag);
			if(!$clTag){
				self::newTag($Tag);
				$clTag = self::getTag($Tag);
			}
			$values[] = "($ID, $clTag->tagID)";
		}
		if($Typ == self::TagMember){
			$SQL = "insert into member_tags (meID, tagID) values " . implode(", ", $values);
		}elseif ($Typ == self::TagEvent){
			$SQL = "insert into event_tags (evID, tagID) values " . implode(", ", $values) . " ON DUPLICATE KEY UPDATE evID = evID, tagID = tagID";
		}
		clMariaDB::query($SQL);

	}
	static function newTag(string $Name) : int {
		clMariaDB::escapeStr($Name);
		$SQL = "insert into tag (tagName) values ('$Name')";
		clMariaDB::query($SQL);
		return clMariaDB::insert_id();
	}
	static function getTag(string $Name){
		clMariaDB::escapeStr($Name);
		$SQL = "select * from tag where tagName = '$Name' ";
		$Tag = clMariaDB::queryObject($SQL, 1 );
		return count($Tag) > 0 ? $Tag[0] : null;
	}
	/*******************Tags***************************/

	/*******************Address***************************/
	static function saveAddressMember(int $ID, object $Address, array $saveFields) : void{
		$addrID = self::getAddress($Address, $saveFields);
		if($addrID == 0) {
			$addrID = self::saveToTable($Address, $saveFields, "address");
		}
		$SQL = "insert into member_address (meID, addID) values ($ID, $addrID)";
		clMariaDB::query($SQL);
	}
	static function saveAddressEvent(int $ID, object $Address, array $saveFields) : void{
		$addrID = self::getAddress($Address, $saveFields);
		if($addrID == 0) {
			$addrID = self::saveToTable($Address, $saveFields, "address");
		}
		$SQL = "insert into event_address (evID, addID) values ($ID, $addrID)";
		clMariaDB::query($SQL);
	}

	static function getAddress(object $Address, array $saveFields){
		$str = "";
		foreach ($saveFields as $dbField => $objField){
			$str .= $Address->{$objField} ?? "";
		}
		//echo $str . " -> " . md5($str) . "\n";
		$SQL = "select addID from address where addMD5 = '" . md5($str) . "'";
		return clMariaDB::Scalar($SQL) ?? 0;
	}
	/*******************Address***************************/
	/*******************Images***************************/
	static function saveImageMember(int $ID, object $Image, array $saveFields) : void{
		$imgID = self::saveToTable($Image, $saveFields, "images");
		$SQL = "insert into member_images (meID, imgID) values ($ID, $imgID)";
		clMariaDB::query($SQL);
	}
	static function saveImageEvent(int $ID, object $Image, array $saveFields) : void{
		$imgID = self::saveToTable($Image, $saveFields, "images");
		$SQL = "insert into event_images (evID, imgID) values ($ID, $imgID)";
		clMariaDB::query($SQL);
	}

	static function saveImage(string $md5, string $image){
		$SQL = "insert ignore into image (imgUrlMD5, image) value ('$md5', '$image')";
		clMariaDB::query($SQL);
	}

	static function hasImage($md5) : bool{
		$SQL = "select count(imgUrlMD5) from image where imgUrlMD5 = '$md5'";
		return clMariaDB::Scalar($SQL) == 1;
	}

	/*******************Images***************************/
	/*******************Location***************************/
	static function saveLocationMember(int $ID, object $Image, array $saveFields) : void{
		$locID = self::saveToTable($Image, $saveFields, "location");
		$SQL = "insert into member_location (meID, locID) values ($ID, $locID)";
		clMariaDB::query($SQL);
	}
	static function saveLocationEvent(int $ID, object $Image, array $saveFields) : void{
		$locID = self::saveToTable($Image, $saveFields, "location");
		$SQL = "insert into event_location (evID, locID) values ($ID, $locID)";
		clMariaDB::query($SQL);
	}
	/*******************Location***************************/
	/*******************Offers***************************/
	static function saveOffersEvent(int $ID, object $Offers, array $saveFields) : void{
		$locID = self::saveToTable($Offers, $saveFields, "offers");
		$SQL = "insert into event_offers (evID, offID) values ($ID, $locID)";
		clMariaDB::query($SQL);
	}
	/*******************Offers***************************/

	static function deleteDBData(){
		$SQL = [];
		foreach (self::$tables as $table){
			$SQL[] = "delete from $table";
			$SQL[] = "ALTER TABLE $table AUTO_INCREMENT = 1";

		}
		clMariaDB::query_multi($SQL);
	}



	static function saveTableDefs(){
		$SQL ="show tables";
		$curDB = clMariaDB::getCurrentDB();
		if (!file_exists(self::$db_def_dir)) {
			mkdir(self::$db_def_dir, 0777, true);
		}

		$tabs = clMariaDB::queryObject($SQL) ?? [];
		$SQL = "
					select fks.table_name as foreign_table,
				   fks.referenced_table_name as primary_table
			from information_schema.referential_constraints fks
			join information_schema.key_column_usage kcu
				 on fks.constraint_schema = kcu.table_schema
				 and fks.table_name = kcu.table_name
				 and fks.constraint_name = kcu.constraint_name
			where fks.constraint_schema = '$curDB'
			group by fks.constraint_schema,
					 fks.table_name,
					 fks.unique_constraint_schema,
					 fks.referenced_table_name,
					 fks.constraint_name
			order by fks.constraint_schema,
					 fks.table_name
					";
		$foreign_key_tables = clMariaDB::queryObject($SQL) ?? [];
		$foreign_table = $primary_table = [];

		foreach ($foreign_key_tables as $ele) {
			$foreign_table[] = $ele->foreign_table;
			$primary_table[] = $ele->primary_table;
		}

		$sort_table = [];
		$i = 0;
		while($i < 1000 && count($primary_table) > 0){
			$i++;
			for($x = 0; $x < count($primary_table); $x++){
				if(!in_array($primary_table[$x], $foreign_table)){
					if(!in_array($primary_table[$x], $sort_table)) {
						$sort_table[] = $primary_table[$x];
					}
					if (($key = array_search($primary_table[$x], $primary_table)) !== false) {
						unset($primary_table[$key]);
						unset($foreign_table[$key]);
						$primary_table = array_values($primary_table);
						$foreign_table = array_values($foreign_table);
					}
					break;
				}
			}
		}


		foreach ($tabs as $tab){
			if(!in_array($tab->{'Tables_in_' . $curDB}, $sort_table)){
				$sort_table[] = $tab->{'Tables_in_' . $curDB};
			}
		}

		for($i = 0; $i < count($sort_table); $i++){
			$create_sql = clMariaDB::queryObject("show create table " . $sort_table[$i])[0];
			$file = self::$db_def_dir . sprintf("%03d_", $i) . $create_sql->Table . ".sql";
			file_put_contents($file, $create_sql->{'Create Table'});
			chmod($file, 0666);
		}
	}

	static function deleteDB(){
		$tables = [];
		foreach (glob(self::$db_def_dir . "*.sql") as $file){
			$tables[] = substr(basename($file),4,-4);
		}

		while (count($tables) > 0){
			$table = array_pop($tables);
			$SQL = "drop table if exists $table";
			clMariaDB::query($SQL);
		}
	}
	static function createDB(){
		$DBName = clMariaDB::getCurrentDB();
		$SQL = "show tables";
		$tabellen = [];
		foreach(clMariaDB::queryObject($SQL) ?? [] as $tab){
			$tabellen[] = $tab->{'Tables_in_' . $DBName};
		}

		foreach (glob(self::$db_def_dir . "*.sql") as $file){
			$tablename = substr(basename($file),4,-4);
			if(!in_array($tablename, $tabellen)){
				$create_sql = file_get_contents($file);
				clMariaDB::query($create_sql);
			}
		}
	}


}