<?php
declare(strict_types = 1);
namespace part\lag\api;
use part\lag\clLAGAPIBase;
use part\lag\db\clLAGDB;
use part\mariadb\clMariaDB;

class clLAGMember extends clLAGAPIBase {

	private object $APIData;
	private string $IDorg;
	private int $ID;
	private static $url_member = "/mitglieder/mitglied/";

	private array $saveFields = [
		"meEmail" => "email",
		"meUrl" => "url",
		"meDescription" => "description",
		"meTelephone" => "telephone",
		"meName" => "name",
		"meFoundingDate" => "foundingDate",
		"meIDorg" => "meIDorg",
		"meTeaser" => "meTeaser",
	];
	public function __construct(object $Data) {
		$this->APIData = $Data;
		$this->IDorg = $this->getIdentifier("ID", $Data)[0];
		$Data->meIDorg = $this->IDorg;
		if(isset($Data->disambiguatingDescription) && is_array($Data->disambiguatingDescription) && count($Data->disambiguatingDescription) > 0){
			$Data->meTeaser = $Data->disambiguatingDescription[0];
		}
		$this->ID = clLAGDB::saveMember($Data, $this->saveFields);
		clLAGDB::saveMerkmaleMember($this->ID, $this->getIdentifier("Merkmale", $Data));
		clLAGDB::saveTagMember($this->ID, $this->getIdentifier("Tag", $Data));
		new clLAGAddress($this->ID, $Data->address ?? [], clLAGAddress::AddrMember);
		new clLAGImage($this->ID, $Data->image ?? [], clLAGImage::ImgMember);
		new clLAGLocation($this->ID, $Data->location ?? [], clLAGLocation::LocMember);
	}

	static function getMember(string $meID) : \stdClass{
		$SQL = "select m.meIDorg, m.meName, m.meTeaser, m.meDescription, m.meUrl, m.meTelephone, m.meEmail,
				imgUrlMD5, imgCreator, imgCaption, imgDescription, imgUrl, tagName,
       			ifnull(av.addID,0) v_addID, av.addStreet v_addStreet, av.addPlZ v_addPlZ, av.addOrt v_addOrt, 
       			ifnull(ap.addID,0) p_addID, ap.addStreet p_addStreet, ap.addPlZ p_addPlZ, ap.addOrt p_addOrt 
				from member m
				left join member_images i on i.meID = m.meID
				left join images im on im.imgID = i.imgID
				left join member_address mav on mav.meID = m.meID 
				left join address av on av.addID = mav.addID and av.addType = 'visitor'    			
    			left join member_address map on map.meID = m.meID 
				left join address ap on ap.addID = map.addID and av.addType = 'post'
				left join member_tags mt on mt.meID = m.meID
                left join tag t on t.tagID = mt.tagID
				where m.meIDorg = '$meID'";
		//cl($SQL);
		$result = clMariaDB::queryObject($SQL) ?? [];

		$member = clMariaDB::queryObject($SQL)[0] ?? new \stdClass();

		$member->page_link = ["url" => self::$url_member . "?id=" . $meID];
		$member->link_button_text = null;
		$member->title = $member->meName ?? "";
		$member->headline = $member->meTeaser ?? "";
		$member->description = $member->meDescription ?? "";
		$member->img = [];
		$member->tags = [];
		$imgs = [];
		foreach ($result as $row) {
			if (isset($row->imgUrlMD5) && !in_array($row->imgUrlMD5, $imgs)) {
				$imgs[] = $row->imgUrlMD5;
				$img = clLAGImage::getImagePostID($row->imgUrlMD5);
				$img['imgCreator'] = $row->imgCreator ?? "";
				$img['imgCaption'] = $row->imgCaption ?? "";
				$img['imgDescription'] = $row->imgDescription ?? "";
				$img['imgUrl'] = $row->imgUrl ?? "";
				$member->img[] = $img;
				$member->image = $img;
			}
			if(isset($row->tagName) && strlen(trim($row->tagName)) > 0) {
				$member->tags[] = $row->tagName;
			}
		}
		$member->tags = array_unique($member->tags);
		return $member;
	}

	static function getMembers() : array{
		$members = [];

		$SQL = "select meIDorg, meTeaser,meName,imgUrlMD5, im.* 
				from member m
				left join (select meID, imgID from member_images group by meID) i on i.meID = m.meID
				left join images im on im.imgID = i.imgID ";
		$result = clMariaDB::queryObject($SQL) ?? [];
		foreach ($result as $row) {
			if (isset($row->imgUrlMD5)) {
				$img = clLAGImage::getImagePostID($row->imgUrlMD5);
				$img['imgCreator'] = $row->imgCreator ?? "";
				$img['imgCaption'] = $row->imgCaption ?? "";
				$img['imgDescription'] = $row->imgDescription ?? "";
				$img['imgUrl'] = $row->imgUrl ?? "";
				$row->image = $img;
			}
			$members[] = $row;
		}

		return $members;
	}



	static function getLocations(){
		$SQL = "select * from member_location ml
			inner join member m on m.meID = ml.meID
			inner join location l on l.locID = ml.locID
			where locType = 'GeoCoordinates'";
		return clMariaDB::queryObject($SQL);
	}
}