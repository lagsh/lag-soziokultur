<?php
declare(strict_types = 1);
namespace part\lag\api;
use part\lag\clLAGAPIBase;
use part\lag\db\clLAGDB;
use part\mariadb\clMariaDB;

class clLAGAddress extends clLAGAPIBase {


	const AddrMember = 1;
	const AddrEvent = 2;

	private array $APIData;
	private int $ID;
	private array $saveFields = [
		"addStreet" => "streetAddress",
		"addPLZ" => "postalCode",
		"addCountry" => "addressCountry",
		"addType" => "addType",
		"addDefault" => "addDefault",
		"addOrt" => "addressLocality",
		"addName" => "addName",
		"addRegion" => "areaServed",
		"addLandkreis" => "addressLocality",
	];
	public function __construct(int $ID, array $Data, int $Typ) {
		$this->APIData = $Data;
		$this->ID = $ID;
		foreach ($Data as $Addr){
			$Addr->addType = $this->getIdentifier("Typ", $Addr)[0] ?? 'post';
			$pref = $this->getIdentifier("Preferred", $Addr);
			$Addr->addDefault = 0;
			if(count($pref) > 0) {
				$Addr->addDefault = $pref[0] == "true" ? 1 : 0;
			}
			if($Typ == self::AddrMember) {
				clLAGDB::saveAddressMember($this->ID, $Addr, $this->saveFields);
			}elseif ($Typ == self::AddrEvent){
				clLAGDB::saveAddressEvent($this->ID, $Addr, $this->saveFields);
			}
		}
	}





	static function getLandkreis(int $Typ) : array{
		switch ($Typ){
			case self::AddrMember:
				$AddrTable = "member_address";
				break;
			case self::AddrEvent:
				$AddrTable = "event_address";
				break;
			default:
				throw new \Exception("unknown Address Typ:$Typ");
		}
		$SQL = "SELECT distinct addLandkreis FROM address a
				inner join $AddrTable ma on ma.addID = a.addID
				inner join event e on e.evID = ma.evID
				where addLandkreis is not null and e.evStartDate >=now()
				order by addLandkreis";

		return clMariaDB::queryObject($SQL);
	}




}