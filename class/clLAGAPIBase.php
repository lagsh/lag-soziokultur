<?php

namespace part\lag;

class clLAGAPIBase {

	protected function getIdentifier(string $Name, object $APIData) : array{
		$ident = [];
		if($APIData->identifier) {
			foreach ($APIData->identifier as $identifier) {
				if ($identifier->name == $Name) {
					$ident[] = trim($identifier->value);
				}
			}
		}
		return $ident;
	}


	protected function convertDate(string $date) : string {
		return (\DateTime::createFromFormat(\DateTime::ISO8601, $date))->format("Y-m-d H:i:s");
	}
}