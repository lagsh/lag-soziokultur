<?php
declare(strict_types = 1);
namespace part\lag\api;
use part\lag\clLAGAPIBase;
use part\lag\db\clLAGDB;
use part\mariadb\clMariaDB;

class clLAGImage extends clLAGAPIBase {
	const ImgMember = 1;
	const ImgEvent = 2;

	private array $APIData;
	private int $ID;
	private array $saveFields = [
		"imgUrl" => "contentUrl",
		"imgLicense" => "license",
		"imgCreator" => "creator",
		"imgCaption" => "caption",
		"imgDescription" => "description",
		"imgUrlMD5" => "imgUrlMD5",
	];
	public function __construct(int $ID, array $Data, int $Typ) {
		$this->APIData = $Data;
		$this->ID = $ID;
		foreach ($Data as $Img){
			$Img->imgUrlMD5 = md5($Img->contentUrl);
			if($Typ == self::ImgMember) {
				clLAGDB::saveImageMember($this->ID, $Img, $this->saveFields);
			}elseif ($Typ == self::ImgEvent){
				clLAGDB::saveImageEvent($this->ID, $Img, $this->saveFields);
			}
			$this->saveImage($Img->contentUrl);
		}
	}



	private function saveImage(string $Url){
		try{
			$md5 = md5($Url);
			if(!clLAGDB::hasImage($md5)) {
				$base64Img = base64_encode(file_get_contents($Url));
				clLAGDB::saveImage($md5, $base64Img);
			}
		}catch (\Exception $ex){
			print $ex->getMessage() ."\n";
		}

	}



	static function importToWordpress(){
		set_time_limit(240);
		$SQL = "SELECT distinct  im.imgID, i.imgUrlMD5, imgUrl FROM image i inner join images im on im.imgUrlMD5=i.imgUrlMD5";
		$images = clMariaDB::queryObject($SQL);
		foreach ($images as $img){
			try{
				self::insertImgToWOrdpress($img->imgUrl, $img->imgUrlMD5);
			}catch (\Exception $ex){
				print "<pre>";
				print_r($ex);
				print "</pre>";
			}
		}
	}


	static function insertImgToWOrdpress(string $image_url, string $md5) : int{
		if(self::MediaFileAlreadyExists($md5)){
			return 0;
		}
		echo "lade $image_url <br>";
		flush();
		ob_flush();
		//return 0;
		$upload_dir = wp_upload_dir();
		$image_data = file_get_contents( $image_url );
		$filename = basename( $image_url );
		if ( wp_mkdir_p( $upload_dir['path'] ) ) {
			$file = $upload_dir['path'] . '/' . $filename;
		}
		else {
			$file = $upload_dir['basedir'] . '/' . $filename;
		}
		file_put_contents( $file, $image_data );

		$file_info = pathinfo($file);

		if(!isset($file_info['extension'])){
			$info = getimagesize($file);
			$new_file = $file;
			if($info['mime']){
				switch ($info['mime']){
					case "image/jpeg": $new_file = $file . ".jpg"; break;
					case "image/png": $new_file = $file . ".png"; break;
					default: error_log("unbekannter Image Typ:{$info['mime']}");
				}
				rename($file, $new_file);
				$file = $new_file;
				$filename = basename( $file );
			}
		};
		$file_info = pathinfo($file);
		//cl($file_info);

		$new_file = $file_info['dirname'] . DIRECTORY_SEPARATOR . $md5 . "." . $file_info['extension'];
		rename($file, $new_file);
		$file = $new_file;
		$filename = basename( $file );

		$wp_filetype = wp_check_filetype( $filename, null );
		$attachment = array(
			'post_mime_type' => $wp_filetype['type'],
			'post_title' => sanitize_file_name( $filename ),
			'post_content' => '',
			'post_status' => 'inherit'
		);

		$attach_id = wp_insert_attachment( $attachment, $file );
		require_once( ABSPATH . 'wp-admin/includes/image.php' );
		$attach_data = wp_generate_attachment_metadata( $attach_id, $file );
		wp_update_attachment_metadata( $attach_id, $attach_data );
		return $attach_id;
	}

	static function MediaFileAlreadyExists($filename){
		global $wpdb;
		$query = "SELECT COUNT(*) FROM {$wpdb->postmeta} WHERE meta_value LIKE '%$filename\.%'";
		return ($wpdb->get_var($query)  > 0) ;
	}


	static function getImagePostID($md5) {
		global $wpdb;

		try{
			$SQL = "SELECT post_id FROM {$wpdb->postmeta} WHERE meta_value LIKE '%$md5\.%' limit 1";
			$post_id = $wpdb->get_var($SQL);
			if(!$post_id){
				return $md5;
			}
			$img = wp_get_attachment_metadata($post_id);
			$url = wp_get_attachment_image_url($post_id);
			if($url === false){
				return ['sizes' => []];
			}
			$dir = dirname($url);
			$ret = [];
			foreach ($img['sizes'] as $key => $val) {
				$ret[$key] = $dir . DIRECTORY_SEPARATOR . $val['file'];
			}
		}catch (\Exception $ex){
			return $ex->getMessage();
		}
		return ['sizes' => $ret];
	}




}