<?php
declare(strict_types = 1);
namespace part\lag\api;

	class clQueryAPICriteria{
		public function __set(string $name, $value) :void {
			$this->{$name} = $value;
		}

		public function getUrlCriteria() : string{
			foreach ($this as $key => $val){
				$criteria[] = urlencode($key) . "=" . urlencode($val);
			}
			return implode("&", $criteria);
		}
	}