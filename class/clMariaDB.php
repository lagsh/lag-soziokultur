<?php
declare(strict_types = 1);
namespace part\mariadb;

class clMariaDB {
	static private $db;
	static function connect(string $host, string $user, string $pw, string $db){
		if(isset(self::$db)){
			return;
		}
		self::$db = new \mysqli($host, $user,$pw, $db );
		if (self::$db->connect_error) {
			/* Use your preferred error logging method here */
			error_log('Connection error: ' . self::$db->connect_error);
			return;
		}
		self::$db->autocommit(true);
		self::$db->query('set character set utf8');
	}

	static function close(){
		if(!isset(self::$db)){
			return;
		}
		self::$db->close();
		self::$db = null;
	}
	static function selectDB(string $dbname){
		if(!self::$db->select_db($dbname)){
			throw new \Exception(self::$db->error);
		}
	}

	static function getCurrentDB() : string{
		return self::Scalar("select database()");
	}

	static function queryObject(string $SQL, int $count = 0, string $className="stdClass") : array{
		$res = self::query($SQL);
		$row = [];
		while ($obj = $res->fetch_object($className)) {
			$row[] = $obj;
			if($count > 0 && count($row) == $count){
				return $row;
			}
		}
		return $row;
	}

	static function query(string $SQL) {
		if(($ret = self::$db->query($SQL)) == false){
			echo $SQL . "\n<br>";
			throw new \Exception(self::$db->error);
		}
		return $ret;
	}


	static function query_multi(array $SQL) : array{
		$results = [];
		if(self::$db->multi_query(implode(";", $SQL)) == false){
			throw new \Exception(print_r(self::$db->error));
		}
		do {
			if ($result = self::$db->store_result()) {
				$results[] = $result;
			}
		} while (self::$db->next_result());
		return $results;
	}

	static function Scalar(string $SQL, $Field = 0){
		return self::query($SQL)->fetch_array()[$Field] ?? null;

	}


	static function escapeStr(string &$Str) : string{
		$Str = self::$db->real_escape_string($Str);
		return $Str;
	}

	static function insert_id() : int{
		return self::$db->insert_id;
	}
}