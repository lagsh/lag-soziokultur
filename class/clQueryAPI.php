<?php
declare(strict_types = 1);
namespace part\lag\api;

class clQueryAPI{
	private string $addrApi = "https://api.kulturadressen.de/api";
	private array $QueryParms = [
		"apiKey" => "4d6c276ac285ac0ed5f73320a3a16f0b",
		"type" => "2328",
		"style" => "json-ld",
		//"fullInfo" => "123",
	];



	public function getData(clQueryAPICriteria $Criteria) : array{
		foreach ($this->QueryParms as $key => $val){
			$Criteria->{$key} = $val;
		}

		$data = file_get_contents($this->addrApi . "?" . $Criteria->getUrlCriteria());

		return json_decode($data);
	}
}