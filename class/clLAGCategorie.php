<?php

namespace part\lag;

use part\mariadb\clMariaDB;

abstract class clLAGCategorie extends clLAGAPIBase {
	static function getCategories():array{
		$SQL = "SELECT distinct c.catID, catName, gemappt FROM categorie c
					inner join event_categorie ec on ec.catID = c.catID
					inner join event e on e.evID = ec.evID
										where evEndDate >= now()
					order by catName";
		return clMariaDB::queryObject($SQL);
	}
}