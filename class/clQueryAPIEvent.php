<?php
declare(strict_types = 1);
namespace part\lag\api;

class clQueryAPIEvent extends clQueryAPI{
	function getData(clQueryAPICriteria $Criteria): array {
		$Criteria->{"search"} = "performance";
		return parent::getData($Criteria);
	}
}