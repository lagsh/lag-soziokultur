<?php

namespace part\lag;

use part\mariadb\clMariaDB;

abstract class clLAGMerkmale extends clLAGAPIBase {
	const MerkmaleMember = 1;
	const MerkmaleEvent = 2;
	static function getMerkmale(int $Typ):array{
		switch ($Typ){
			case self::MerkmaleMember:
				$MerkmalTable = "member_merkmale";
				break;
			case self::MerkmaleEvent:
				$MerkmalTable = "event_merkmale";
				break;
			default:
				throw new \Exception("unknown Address Typ:$Typ");
		}

		$SQL = "SELECT distinct merName, m.merID FROM merkmale m
			inner join $MerkmalTable em on m.merID = em.merID
			inner join event e on e.evID = em.evID
			where evEndDate >= now()
			order by merName";
		return clMariaDB::queryObject($SQL);
	}
}