<?php

namespace part\lag;

use part\mariadb\clMariaDB;

abstract class clLAGTag extends clLAGAPIBase {
	const TagTypMember = 1;
	const TagTypEvent = 2;
	static function getTags(int $Typ):array{
		switch ($Typ){
			case self::TagTypMember:
				$TagTable = "member_tags";
				break;
			case self::TagTypEvent:
				$TagTable = "event_tags";
				break;
			default:
				throw new \Exception("unknown TagTyp:$Typ");
		}
		$SQL = "SELECT distinct t.tagID, tagName FROM tag t
				inner join $TagTable et on et.tagID = t.tagID
				inner join event e on e.evID = et.evID
					where evEndDate >= now()
				order by tagName";

		return clMariaDB::queryObject($SQL);
	}
}