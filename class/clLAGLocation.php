<?php
declare(strict_types = 1);
namespace part\lag\api;
use part\lag\clLAGAPIBase;
use part\lag\db\clLAGDB;
use part\mariadb\clMariaDB;

class clLAGLocation extends clLAGAPIBase {
	const LocMember = 1;
	const LocEvent = 2;

	private array $APIData;
	private int $ID;
	private array $saveFields = [
		"locLng" => "longitude",
		"locLat" => "latitude",
		"locType" => "@type",
		"locName" => "name",
		"locUrl" => "url",
		"locDescription" => "description",
	];
	public function __construct(int $ID, array $Data, int $Typ) {
		$this->APIData = $Data;
		$this->ID = $ID;
		$saveFunc = $Typ == self::LocMember ? function($a, $b, $c) {clLAGDB::saveLocationMember($a, $b, $c);} : function($a, $b, $c) {clLAGDB::saveLocationEvent($a, $b, $c);};
		foreach ($Data as $Loc){
			switch ($Loc->{'@type'}){
				case 'Place':
					if($Loc->geo->latitude > 0 && $Loc->geo->longitude > 0) {
						$saveFunc($this->ID, $Loc->geo, $this->saveFields);
					}
					break;
				default:
					$saveFunc($this->ID, $Loc, $this->saveFields);
			}
		}
	}









}