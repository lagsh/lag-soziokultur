<?php

/*
Plugin Name: PART LAG Soziokultur API
Plugin URI:  https://part.berlin/
Description: A plugin to Import und Expose API Data
Version:     0.1
Author:      PART GmbH für digitales Handeln
Author URI:  https://part.berlin/
License:     MIT
License URI: https://opensource.org/licenses/MIT
Text Domain: part-lag-soziokultur-data
Domain Path: /languages
*/

include_once "include.php";

use part\lag\api\clLAGImage;
use part\lag\api\clLAGMember;
use part\lag\api\clQueryAPICriteria;
use part\lag\api\clQueryAPIEvent;
use part\lag\api\clQueryAPIMembers;
use part\lag\clLAGCategorie;
use part\lag\clLAGMerkmale;
use part\lag\db\clLAGDB;
use part\mariadb\clMariaDB;
use part\lag\clLAGTag;
use part\lag\api\clLAGAddress;
use part\lag\api\clLAGEvent;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


new lag_data_plugin_setup();

class lag_data_plugin_setup{

	private string $cronJobName = "lag_data_get_api_data_cron";
	function __construct(){
		register_activation_hook( __FILE__, array( $this, 'createCronJob' ) );
		register_activation_hook( __FILE__, array( $this, 'createTables' ) );
		register_deactivation_hook( __FILE__, array( $this, 'deleteCronJob' ) );
		register_deactivation_hook( __FILE__, array( $this, 'deleteTables' ) );
		add_action($this->cronJobName, array( $this, 'getLAG_API_Data' ));
		// Create Ajax Endpoint for map location overlay
		add_action( 'wp_ajax_part_lag_member_detail', [$this,'part_lag_member_detail'] );
		add_action( 'wp_ajax_nopriv_part_lag_member_detail', [$this,'part_lag_member_detail']);
		add_action( 'wp_ajax_part_lag_search_events', [$this,'part_lag_search_events'] );
		add_action( 'wp_ajax_nopriv_part_lag_search_events', [$this,'part_lag_search_events'] );
		add_shortcode( "part_lag_data_event_tags", [$this,"getEventTags"]);
		add_shortcode( "part_lag_data_event_regions", [$this,"getRegion"]);
		add_shortcode( "part_lag_data_event_categories", [$this,"getCategories"]);
		add_shortcode( "part_lag_data_event_merkmale", [$this,"getMerkmale"]);
		clMariaDB::connect(DB_HOST, "root", DB_PASSWORD, DB_NAME);
	}

	function __destruct(){
		clMariaDB::close();
	}

	function createCronJob(){
		if (! wp_next_scheduled ( $this->cronJobName ) ) {
			wp_schedule_event(time(), 'daily', $this->cronJobName);
		}
	}

	function deleteCronJob(){
		if ( wp_next_scheduled( $this->cronJobName ) ) {
			wp_clear_scheduled_hook( $this->cronJobName );
		}
	}

	function createTables(){
		error_log("Datenbank Tabellen werden angelegt.");
		clLAGDB::createDB();
	}

	function deleteTables(){
		error_log("Datenbank Tabellen werden gelöscht.");
		clLAGDB::deleteDB();
	}


	function part_lag_member_detail () {
		$data = new stdClass();
		$data = clLAGMember::getMember($_POST['post_id']);
		echo json_encode($data);
		die();
	}

	function part_lag_search_events () {
		//\part\lag\api\clLAGImage::importToWordpress();
		$parms = json_decode(base64_decode($_POST['request']));
		$data = clLAGEvent::searchEvents($parms);
		echo json_encode($data);
		die();
	}


	function getEventTags() {
		$Links = [];
		$tags = clLAGTag::getTags(clLAGTag::TagTypEvent);
		foreach ($tags as $tag){
			$tag->typ = clLAGTag::TagTypEvent;
			$Links[] = "<a href='#' data-tagid='$tag->tagID' data-tag-typ='{$tag->typ} 'class='ui button category'>{$tag->tagName}</a>";
		}
		return implode("\n", $Links);
	}


	function getRegion() {
		$Options = [];
		$regions = clLAGAddress::getLandkreis(clLAGAddress::AddrEvent);
		foreach ($regions as $region){
			$region->typ = clLAGAddress::AddrEvent;
			$Options[] = "<option value='{$region->addLandkreis}' data-tag-typ='{$region->typ}'>{$region->addLandkreis}</option>";
		}
		return implode("\n", $Options);
	}

	function getCategories() {
		$Options = [];
		$Categories = clLAGCategorie::getCategories();
		foreach ($Categories as $Cat){
			$Options[] = "<option value='{$Cat->catID}'>{$Cat->catName}</option>";
		}
		return implode("\n", $Options);
	}


	function getMerkmale() {
		$Options = [];
		$merkmale = clLAGMerkmale::getMerkmale(clLAGMerkmale::MerkmaleEvent);
		foreach ($merkmale as $merkmal){
			$merkmal->typ = clLAGMerkmale::MerkmaleEvent;
			$Options[] = "<option value='{$merkmal->merID}' data-tag-typ='{$merkmal->typ}'>{$merkmal->merName}</option>";
		}
		return implode("\n", $Options);
	}


	function getLAG_API_Data(){
		try{
			error_log("Import Data from LAG API ");
			clLAGDB::deleteDBData();
			$c = new clQueryAPICriteria();
			$q = new clQueryAPIMembers();

			$data = $q->getData($c);

			foreach ($data as $member) {
				new clLAGMember($member);
			}

			$c = new clQueryAPICriteria();
			$q = new clQueryAPIEvent();

			$data = $q->getData($c);
			foreach ($data as $event) {
				new clLAGEvent($event);
			}
			clLAGImage::importToWordpress();
			error_log("Imort Data erfolgreich");
		}catch (\Exception $ex){
			error_log("Import LAG Data fehlgeschlagen:" . $ex->getMessage());
		}
	}


}




//var_dump(DB_NAME, DB_USER, DB_PASSWORD, DB_HOST);
function part_lag_connect(){
	clMariaDB::connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
}


function holeMarker(){
	part_lag_connect();
	$members = clLAGMember::getLocations();
	$markers = [];
	foreach ($members as $member){
		$mark = new stdClass();
		$mark->icon= "/wp-content/uploads/map-pin-default.svg";
		$mark->icon_active = "/wp-content/uploads/map-pin-default-active.svg";
		$mark->geolocation = new stdClass();
		$mark->geolocation->lat = $member->locLat;
		$mark->geolocation->lng = $member->locLng;
		$mark->click = "part_map_click_action";

		$mark->id = $member->meIDorg;

		$markers[] = $mark;
	}

	return $markers;
}



/*// Create Ajax Endpoint for map location overlay
add_action( 'wp_ajax_part_lag_member_detail', 'part_lag_member_detail' );
add_action( 'wp_ajax_nopriv_part_lag_member_detail', 'part_lag_member_detail' );
if (! function_exists('part_lag_member_detail')) {
	function part_lag_member_detail () {
		part_lag_connect();
		$data = new stdClass();
		$data = clLAGMember::getMember($_POST['post_id']);
		clMariaDB::close();
		echo json_encode($data);
		die();
	}
}


// Create Ajax Endpoint for map location overlay
add_action( 'wp_ajax_part_lag_search_events', 'part_lag_search_events' );
add_action( 'wp_ajax_nopriv_part_lag_search_events', 'part_lag_search_events' );
if (! function_exists('part_lag_search_events')) {
	function part_lag_search_events () {
		part_lag_connect();
		//\part\lag\api\clLAGImage::importToWordpress();
		$parms = json_decode(base64_decode($_POST['request']));
		$data = clLAGEvent::searchEvents($parms);
		echo json_encode($data);
		clMariaDB::close();
		die();
	}
}


//Eventverteiler
add_shortcode( "part_lag_data_event_tags", "getEventTags");
if (! function_exists('getEventTags')) {
	function getEventTags() {
		part_lag_connect();
		$Links = [];
		$tags = clLAGTag::getTags(clLAGTag::TagTypEvent);
		foreach ($tags as $tag){
			$tag->typ = clLAGTag::TagTypEvent;
			$Links[] = "<a href='#' data-tagid='$tag->tagID' data-tag-typ='{$tag->typ} 'class='ui button category'>{$tag->tagName}</a>";
		}
		return implode("\n", $Links);
	}
}

add_shortcode( "part_lag_data_event_regions", "getRegion");
if (! function_exists('getRegion')) {
	function getRegion() {
		part_lag_connect();
		$Options = [];
		$regions = clLAGAddress::getLandkreis(clLAGAddress::AddrEvent);
		foreach ($regions as $region){
			$region->typ = clLAGAddress::AddrEvent;
			$Options[] = "<option value='{$region->addLandkreis}' data-tag-typ='{$region->typ}'>{$region->addLandkreis}</option>";
		}
		return implode("\n", $Options);
	}
}
add_shortcode( "part_lag_data_event_categories", "getCategories");
if (! function_exists('getCategories')) {
	function getCategories() {
		part_lag_connect();
		$Options = [];
		$Categories = clLAGCategorie::getCategories();
		foreach ($Categories as $Cat){
			$Options[] = "<option value='{$Cat->catID}'>{$Cat->catName}</option>";
		}
		return implode("\n", $Options);
	}
}

add_shortcode( "part_lag_data_event_merkmale", "getMerkmale");
if (! function_exists('getMerkmale')) {
	function getMerkmale() {
		part_lag_connect();
		$Options = [];
		$merkmale = clLAGMerkmale::getMerkmale(clLAGMerkmale::MerkmaleEvent);
		foreach ($merkmale as $merkmal){
			$merkmal->typ = clLAGMerkmale::MerkmaleEvent;
			$Options[] = "<option value='{$merkmal->merID}' data-tag-typ='{$merkmal->typ}'>{$merkmal->merName}</option>";
		}
		return implode("\n", $Options);
	}
}
*/