<?php
spl_autoload_register('part_autoloader');
define('MY_ROOT', $_SERVER['DOCUMENT_ROOT'] . "/");
setlocale(LC_ALL,"de_DE.utf8");
ini_set('include_path',
	MY_ROOT . "/wp-content/plugins/part-lag-soziokultur-data/class/:"
);


function part_autoloader($class) {
	$namespace = 'part\\';

	if (strpos($class, $namespace) !== 0) {
		return;
	}
	
	try {
		$class = explode('\\',$class);
		$class = end($class);
		//error_log("oeffne ". $class . '.php');
		include_once ( $class . '.php');
	} catch (Exception $e) {
		try {
			error_log("öffne ". $class . '.inc');
			include_once ( $class . '.inc');
		} catch (Exception $e) {
			throw $e;
		}
	}
}